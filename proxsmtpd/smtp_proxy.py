## Reference - https://pymotw.com/2/smtpd/
## Author Denis Tregubov, 2017

import smtpd
import re

class SMTPProxy(smtpd.SMTPServer):
    """ Main class for SMTP proxy """

    def process_message(self, peer, mailfrom, rcpttos, data):
        """ Modified process_message method.
            Can process headers of the data and act accordingly.
        """
        headers = self.get_headers(data)
        print headers

        return

    def get_headers(self, data):
        """ Split data string into list if it matches header regex.
            Then list gets splitted by keys and values and put into dict.
            return dictionary
        """

        headers_dict = {}
        headers = [x for x in data.split('\n') if re.search("(.*[a-zA-Z])\:\ (.*)", x)]

        for header in headers:
            item = header.split(":")
            key = item[0]
            value = item[1]
            headers_dict[key] = value

        return headers_dict
