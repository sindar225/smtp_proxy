from proxsmtpd.smtp_proxy import SMTPProxy
import asyncore

if __name__ == "__main__":
    LOCAL_ADDR = "localhost"
    LOCAL_IN_PORT = 2525

    smtp = SMTPProxy((LOCAL_ADDR, LOCAL_IN_PORT), None)

    asyncore.loop()
